/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.grafos;

/**
 *
 * @author Usuario
 */
public class Cola {

    ListaLigada lista;

    public Cola() {
        lista = new ListaLigada();
    }

    public boolean esVacia() {
        return lista.esVacia();
    }

    public void encolar(int e, int s, int pr) {
        lista.insertaNodos(e, s, pr);
    }

    public NodoLista desencolar() {
        return lista.deletePrimerNodo();
    }
}
