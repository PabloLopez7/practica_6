/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.grafos;

import java.awt.Graphics;

/**
 *
 * @author Usuario
 */

public interface Dibujable {

    public boolean estaDentro(int x, int y);
    public void setPosicion(int x, int y);
    public int getX();
    public int getY();
    public void paint(Graphics g);
}
