/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.tda.grafos;

import java1.uac.Grafo;
import java1.uac.LimiteException;

/**
 *
 * @author Usuario
 */
public class Dijkstra {

    private int costos[][];
    private int ultimo[];
    private int D[];
    private boolean F[];
    private int s, n;

    public Dijkstra(int s, Grafo<Integer> g) throws LimiteException {
        n = g.ordenGrafo();
        this.s = s;
        costos = insMatriz(g);
        ultimo = new int[n];
        D = new int[n];
        F = new boolean[n];
    }

    public Dijkstra(controlador.grafos.Grafo grafo, Integer parser, Integer parser0) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    private int[][] insMatriz(Grafo<Integer> g) throws LimiteException {
        int t = g.ordenGrafo();
        costos = new int[t][t];
        for (int i = 0; i < t; i++) {
            for (int j = 0; j < t; j++) {
                costos[i][j] = g.costoArco(i, i);
            }
        }
        return costos;
    }

    public void caminoMinimos() {
        for (int i = 0; i < n; i++) {
            F[i] = false;
            D[i] = costos[s][i];
            ultimo[i] = s;
        }
        F[s] = true;
        D[s] = 0;

        for (int i = 0; i < n; i++) {
            int v = minimo();
            F[v] = true;
            for (int w = 0; w < n; w++) {
                if (!F[w]) {
                    if ((D[v] + costos[v][w]) < D[w]) {
                        D[w] = D[v] + costos[v][w];
                        ultimo[w] = v;
                    }
                }
            }
        }
        for (int i = 0; i < n; i++) {
            System.out.println("costo minimo a " + i + ":" + D[i]);
        }
    }

    public int minimo() {
        int mx = Grafo.inf;
        int v = 1;
        for (int j = 0; j < n; j++) {
            if (!F[j] && (D[j] <= mx)) {
                mx = D[j];
                v = j;
            }
        }
        return v;
    }

    public void recuperaCamino(int j) {
        int anterior = ultimo[j];
        if (j!= s) {
            recuperaCamino(anterior); // vuelve al último del último
            System.out.print(" -> J" + j);
        } else {
            System.out.print("J" + s);
        }
    }

    public String empezar() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
