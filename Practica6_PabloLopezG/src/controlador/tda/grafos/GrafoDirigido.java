/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.tda.grafos;

import controlador.tda.grafos.exception.VerticeException;
import controlador.tda.lista.ListaEnlazada;

/**
 *
 * @author Usuario
 */
public class GrafoDirigido extends Grafos {
    protected Integer numeroVertice;
    protected Integer numeroArista;
    protected ListaEnlazada<Adycencia> listaAdycente[];
    
    public GrafoDirigido(Integer numeroVertice){
        this.numeroVertice=numeroVertice;
        this.numeroArista=0;
        listaAdycente= new ListaEnlazada[numeroVertice + 1];
        for(int i=1; i<=this.numeroVertice; i++){
            listaAdycente[i]= new ListaEnlazada<>();
        }
    }

    @Override
    public Integer numeroVertices() {
        return this.numeroVertice;
    }

    @Override
    public Integer numeroAristas() {
        return this.numeroArista;
    }

    @Override
    public Object[] existeArista(Integer i, Integer f) throws VerticeException {
       Object[] resultado={Boolean.FALSE, Double.NaN};
       if(i>0 && f>0 && i<= numeroVertice && f<= numeroVertice){
           ListaEnlazada<Adycencia> lista = listaAdycente[i];
           for(int m=0; m<lista.getSize(); m++){
              try{
                  Adycencia aux = lista.obtenerDato(m);
                  if(aux.getDestino().intValue()==f.intValue()){
                      resultado[0]=Boolean.TRUE;
                      resultado[1]=aux.getPeso();
                      break;
                  }
              }catch(Exception e){
              }
           }
           return resultado;
       }else
           throw new VerticeException("Algun Vertice Ingresado No Existe");
    }

    @Override
    public Double pesoArista(Integer i, Integer f) throws VerticeException {
        Double peso = Double.NaN;
        Object [] existe = existeArista(i,f);
        if((Boolean)(existe[0])){
            peso=(Double)existe[1];
        }
        return peso;
    }

    @Override
    public void insertarArista(Integer i, Integer j) throws VerticeException {
        insertarArista(i,j,Double.NaN);
    }

    @Override
    public void insertarArista(Integer i, Integer j, Double peso) throws VerticeException {
        if(i>0 && j>0 && i<= numeroVertice && j<= numeroVertice){
            Object [] existe = existeArista(i,j);
            if(!(Boolean)(existe[0])){
                numeroArista++;
                listaAdycente[i].insertarCabecera(new Adycencia(j,peso));
            }
        }else
            throw new VerticeException("Algun Vertice Ingresado No Existe");
    }

    @Override
    public ListaEnlazada<Adycencia> adycentes(Integer i) throws VerticeException {
        return listaAdycente[i];
    }
    
    public static void main(String[]args){
        GrafoDirigido grafo= new GrafoDirigido(8);
        
        try{
            grafo.insertarArista(2, 6);
            grafo.insertarArista(1, 8);
            grafo.insertarArista(4, 6);
            System.out.println(grafo.existeArista(1, 8)[0]);
            System.out.println(grafo.toString());
        }catch(Exception e){
            System.out.println("Error" + e);
            
        }
    }
}
