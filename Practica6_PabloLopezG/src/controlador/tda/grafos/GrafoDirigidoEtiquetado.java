/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.tda.grafos;

import controlador.tda.grafos.exception.VerticeException;
import controlador.tda.lista.ListaEnlazada;
import java.util.HashMap;
/**
 *
 * @author Usuario
 */
public class GrafoDirigidoEtiquetado<E> extends GrafoDirigido {

    protected Class clazz;
    protected E etiquetas[];
    protected HashMap<E, Integer> dicVertices;

    public GrafoDirigidoEtiquetado(Integer numeroVertice, Class clazz) {
        super(numeroVertice);
        this.clazz = clazz;
        etiquetas = (E[]) java.lang.reflect.Array.newInstance(this.clazz, numeroVertice + 1);
        dicVertices = new HashMap<>(numeroVertice);
    }

    public Object[] existeAristaE(E i, E j) throws Exception {
        return this.existeArista(obtenerCodigo(i), obtenerCodigo(j));
    }

    public void insertarAristaE(E i, E j, Double peso) throws Exception {
        this.insertarArista(obtenerCodigo(i), obtenerCodigo(j), peso);
    }

    public void insertarAristaE(E i, E j) throws Exception {
        this.insertarArista(obtenerCodigo(i), obtenerCodigo(j), Double.NaN);
    }

    public Integer obtenerCodigo(E etiqueta) throws Exception {
        Integer key = dicVertices.get(etiqueta); 
        if(key != null)
            return key;
        else
            throw new VerticeException("NO SE ENCUENTRA LA ETIQUETA");
    }

    public E obtenerEtiqueta(Integer codigo) throws Exception {
        return etiquetas[codigo];
    }

    public ListaEnlazada<Adycencia> adycenteDEE(E i) throws Exception {
        return adycentes(obtenerCodigo(i));
    }

    public void etiquetarVertice(Integer codigo, E etiqueta) {
        etiquetas[codigo] = etiqueta;
        dicVertices.put(etiqueta, codigo);
    }

    @Override
    public String toString() {
        StringBuilder grafo = new StringBuilder();
        try {
            for (int i = 1; i <= numeroVertices(); i++) {
                grafo.append("Vertice" + " " + i + "--E--" + obtenerEtiqueta(i).toString());
                try {
                    ListaEnlazada<Adycencia> lista = adycentes(i);
                    for (int j = 0; j < lista.getSize(); j++) {
                        Adycencia aux = lista.obtenerDato(j);
                        if (aux.getPeso().toString().equalsIgnoreCase(String.valueOf(Double.NaN))) {
                            grafo.append("<----Vertice Destino--->" + aux.getDestino() + " --E--" + obtenerEtiqueta(aux.getDestino()));
                        } else {
                            grafo.append("<----VERTICE DESTINO--->" + aux.getDestino() + " --E--" + obtenerEtiqueta(aux.getDestino()) + "**PESO**" + aux.getPeso());
                        }
                    }
                    grafo.append("\n");
                } catch (Exception e) {
                    System.out.println("Error en toString" + e);

                }
            }
        } catch (Exception e) {
            System.out.println("Error en toString " + e);
        }
        return grafo.toString();
    }

    public static void main(String[] args) {
        GrafoDirigidoEtiquetado<String> gde = new GrafoDirigidoEtiquetado<>(5, String.class);
        gde.etiquetarVertice(1, "Pablo");
        gde.etiquetarVertice(2, "Juan");
        gde.etiquetarVertice(3, "Luis");
        gde.etiquetarVertice(4, "Karla");
        gde.etiquetarVertice(5, "Maria");

        try {
            gde.insertarAristaE("Pablo", "Luis");
            gde.insertarAristaE("karla", "Maria");
        } catch (Exception e) {
            System.out.println("Error" + e);
        }
        System.out.println(gde.toString());
    }
}
