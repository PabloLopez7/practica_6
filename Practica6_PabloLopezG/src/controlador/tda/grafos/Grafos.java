/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.tda.grafos;

import controlador.tda.grafos.exception.VerticeException;
import controlador.tda.lista.ListaEnlazada;

public abstract class Grafos {

    public abstract Integer numeroVertices();
    public abstract Integer numeroAristas();
    public abstract Object[] existeArista(Integer i, Integer f) throws VerticeException;
    public abstract Double pesoArista(Integer i, Integer f) throws VerticeException;
    public abstract void insertarArista(Integer i, Integer j) throws VerticeException;
    public abstract void insertarArista(Integer i, Integer j, Double peso) throws VerticeException;
    public abstract ListaEnlazada<Adycencia> adycentes(Integer i) throws VerticeException;

    @Override
    public String toString() {
        StringBuilder grafo = new StringBuilder();
        for (int i = 1; i <= numeroVertices(); i++) {
            grafo.append("Vertice" + " " + i);
            try {
                ListaEnlazada<Adycencia> lista = adycentes(i);
                for (int j = 0; j < lista.getSize(); j++) {
                    Adycencia aux = lista.obtenerDato(j);
                    if (aux.getPeso().toString().equalsIgnoreCase(String.valueOf(Double.NaN))) {
                        grafo.append("<----Vertice Destino--->" + aux.getDestino());
                    } else {
                        grafo.append("<----VERTICE DESTINO--->" + aux.getDestino() + "**PESO**" + aux.getPeso());
                    }
                }
                grafo.append("\n");
            } catch (Exception e) {
                System.out.println("Error en toString" + e);

            }
        }
        return grafo.toString();
    }

    
}
