/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.tda.grafos;

import java.util.Scanner;
import java1.uac.Grafo;
import java1.uac.GrafoCol;
import java1.uac.LimiteException;

/**
 *
 * @author Usuario
 */
public class GrafosNumeros {

    Grafo<Integer> crear() throws LimiteException {
        Grafo<Integer> g = new GrafoCol<>();
        g.insVertice(0);
        g.insVertice(1);
        g.insVertice(2);
        g.insVertice(3);
        g.insVertice(4);
        
        g.insArco(0, 1, 55);
        g.insArco(0, 2, 110);
        g.insArco(0, 3, 79);
        
        g.insArco(1, 3, 70);
        
        g.insArco(2, 0, 123);
        g.insArco(2, 3, 40);
        g.insArco(2, 4, 180);
        
        g.insArco(3, 2, 89);
        g.insArco(3, 4, 105);
        
        return g;
    }
    
    public static void main(String[] args) throws LimiteException {
        GrafosNumeros gn = new GrafosNumeros();
        Grafo<Integer> crear = gn.crear();
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el vertice origen1: ");
        int origen = sc.nextInt();
        Dijkstra camino = new Dijkstra(origen, crear);
        camino.caminoMinimos();
        System.out.println("Ingrese la ruta en la que quiere llegar");
        int ruta = sc.nextInt();
        camino.recuperaCamino(ruta);
        
    }
}
