/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador.tda.grafos;

/**
 *
 * @author Usuario
 */
public class VerticeAdyacente {
    private String nombre;
    private VerticeAdyacente sig;
    private VerticeAdyacente ant;
    private int estado;
    
    public VerticeAdyacente(String nombre){
        this.nombre=nombre;
        sig= ant=null;
        estado=0;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public VerticeAdyacente getSig() {
        return sig;
    }

    public void setSig(VerticeAdyacente sig) {
        this.sig = sig;
    }

    public VerticeAdyacente getAnt() {
        return ant;
    }

    public void setAnt(VerticeAdyacente ant) {
        this.ant = ant;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
    
    @Override 
    public String toString(){
        return nombre +"->";
    }
}
