/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package vista.grafo.modelo;

import controlador.tda.grafos.GrafoDirigido;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Usuario
 */
public class ModeloTablaGrafo extends AbstractTableModel {

    private GrafoDirigido grafoDirigido;
    private String[] columnas;

    private String[] generarColumnas() {
        columnas = new String[grafoDirigido.numeroVertices() + 1];
        columnas[0] = "/";
        for (int i = 1; i < columnas.length; i++) {
            columnas[i] = String.valueOf(i);
        }
        return columnas;
    }

    public GrafoDirigido getGrafoDirigido() {
        return grafoDirigido;
    }

    public void setGrafoDirigido(GrafoDirigido grafoDirigido) {
        this.grafoDirigido = grafoDirigido;
        generarColumnas();
    }

    public String[] getColumnas() {
        return columnas;
    }

    public void setColumnas(String[] columnas) {
        this.columnas = columnas;
    }

    @Override
    public int getRowCount() {
        return grafoDirigido.numeroVertices();
    }

    @Override
    public int getColumnCount() {
        return grafoDirigido.numeroVertices() + 1;
    }

    @Override
    public String getColumnName(int column) {
        return columnas[column];
    }

    @Override
    public Object getValueAt(int arg0, int arg1) {
        if (arg1 == 0) {
            return columnas[arg0 + 1];
        } else {
            try {
                Object[] aux = grafoDirigido.existeArista((arg0 + 1), arg1);
                if ((Boolean) (aux[0])) {
                    return aux[1];
                } else {
                    return "-----";
                }
            } catch (Exception e) {
                System.out.println("Error en tabla " +e);
                return "";
            }
        }
    }

}
